//
//  GameProductView.swift
//  RappiDom
//
//  Created by Ricardo Grajales Duque on 10/19/18.
//  Copyright © 2018 Ricardo Grajales Duque. All rights reserved.
//

import UIKit

public enum AnimationType {
    case like
    case nonLike
    case superLike
}

class GameProductView: UIView {

    @IBOutlet var contentView: UIView!
    @IBOutlet var contentGameAnimationView: UIView!
    @IBOutlet var productView: UIView!
    @IBOutlet var productImageView: UIImageView!
    @IBOutlet var gameAnimationImageView: UIImageView!
    @IBOutlet var endGameButton: UIButton!
    public var gameProductDelegate: GameProductDelegate?
    private var panGesture  = UIPanGestureRecognizer()
    private var originalProductViewFrame: CGRect?
    private var products: [Product]?
    private var currentProduct: Product?
    private var currentProductCount = 0


    override init(frame: CGRect) {
        super.init(frame: frame)
        setUpView()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setUpView()
    }
    
    private func setUpView() {
        Bundle.main.loadNibNamed("GameProductView", owner: self, options: nil)
        addSubview(contentView)
        contentView.frame = self.bounds
        contentView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        originalProductViewFrame = productView.frame
        let underlineAttribute = [NSAttributedString.Key.underlineStyle: NSUnderlineStyle.single.rawValue]
        let underlineAttributedString = NSAttributedString(string: "Terminar", attributes: underlineAttribute)
        endGameButton.titleLabel!.attributedText = underlineAttributedString
        products = GameViewController.getProducs()
        setUpGameAnimation()
    }
    
    private func setUpGameAnimation() {
        let imageName = currentProduct?.imageName ?? products?[0].imageName
        if (GameViewController.getImage(with: imageName ?? "") != nil) {
            productImageView.image = GameViewController.getImage(with: imageName ?? "")
        }
        panGesture = UIPanGestureRecognizer(target: self, action: #selector(GameProductView.dragFuction(_:)))
        contentView.addGestureRecognizer(panGesture)
    }
    
    @objc func dragFuction(_ sender: UIPanGestureRecognizer) {
        let translation = sender.translation(in: self)
        let centerPoint = CGPoint(x: productView.center.x + translation.x, y: productView.center.y + translation.y)
        
        if (sender.state == UIPanGestureRecognizer.State.ended) {
            endingGameAnimation(centerPoint: centerPoint)
        }
        else {
            productView.center = centerPoint
            sender.setTranslation(CGPoint.zero, in: self)
        }
    }
    
    private func endingGameAnimation(centerPoint: CGPoint) {
        
        if (centerPoint.y + productView.frame.height <= contentGameAnimationView.frame.height/2) {
            translateAnimation(translationX: centerPoint.x, translationY: -contentGameAnimationView.frame.height, animationType: .superLike)
        }
        else if (centerPoint.x <= contentGameAnimationView.frame.width/2) {
            translateAnimation(translationX: -self.frame.width, translationY: centerPoint.y, animationType: .nonLike)
        }
        else if (centerPoint.x >= contentGameAnimationView.frame.width/2) {
            translateAnimation(translationX: self.frame.width, translationY: centerPoint.y, animationType: .like)
        }
    }
    
    private func translateAnimation(translationX: CGFloat, translationY: CGFloat, animationType: AnimationType) {
        UIView.animate(withDuration: 0.35, animations: {
            self.productView.transform = CGAffineTransform.init(translationX: translationX, y: translationY)
        }) { (isCompleted) in
            
            if isCompleted {
                self.iconAnimation(animationType: animationType)
            }
        }
    }
    
    private func iconAnimation(animationType: AnimationType) {
        var animationImage: UIImage?
        switch animationType {
        case .like:
            animationImage = UIImage.init(named: "like-icon")
            break
        case .nonLike:
            animationImage = UIImage.init(named: "nonLike-icon")
            break
        case .superLike:
            animationImage = UIImage.init(named: "superLike-icon")
            break
        }
        gameAnimationImageView.image = animationImage
        UIView.animate(withDuration: 0.35, animations: {
            self.gameAnimationImageView.alpha = 1.0
        }) { (isCompleted) in
            if isCompleted {
                UIView.animate(withDuration: 0.45, animations: {
                    self.gameAnimationImageView.alpha = 0.0
                }) { (isCompleted) in
                    if isCompleted {
                        self.productView.transform = CGAffineTransform.identity
                        self.productView.frame = self.originalProductViewFrame!
                        self.productView.layoutIfNeeded()
                        self.manageProductCount()
                        self.gameProductDelegate?.rateProduct(with: GameViewController.userProfile!, animationType: animationType, product: self.currentProduct!)
                    }
                }
            }
        }
    }
    
    private func manageProductCount() {
        self.currentProductCount += 1
        self.currentProduct = self.products?[self.currentProductCount]
        let imageName = currentProduct?.imageName ?? products?[self.currentProductCount].imageName
        if (GameViewController.getImage(with: imageName ?? "") != nil) {
            productImageView.image = GameViewController.getImage(with: imageName ?? "")
        }
    }
    
    @IBAction func endGameAction(_ sender: Any) {
        gameProductDelegate?.endGameAction()
    }
    
}

protocol GameProductDelegate {
    func endGameAction()
    func rateProduct(with userProfile: UserProfile, animationType: AnimationType, product: Product)
}


