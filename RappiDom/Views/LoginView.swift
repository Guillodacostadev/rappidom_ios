//
//  LoginView.swift
//  RappiDom
//
//  Created by Ricardo Grajales Duque on 10/19/18.
//  Copyright © 2018 Ricardo Grajales Duque. All rights reserved.
//

import UIKit
import FBSDKCoreKit
import FBSDKLoginKit
import Lottie

class LoginView: UIView {
    
    @IBOutlet var contentView: UIView!
    @IBOutlet var loginAnimationView: UIView!
    @IBOutlet var loginButton: UIButton!
    public var loginDelegate: LoginDelegate?

    override init(frame: CGRect) {
        super.init(frame: frame)
        setUpView()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setUpView()
    }
    
    private func setUpView() {
        
        Bundle.main.loadNibNamed("LoginView", owner: self, options: nil)
        addSubview(contentView)
        contentView.frame = self.bounds
        contentView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        
        setUpAnimation()
    }
    
    private func setUpAnimation() {
        let animationView = LOTAnimationView(name: "login_animation")
        animationView.frame = CGRect(x: 0, y: 0, width: loginAnimationView.frame.width, height: loginAnimationView.frame.height)
        animationView.contentMode = .scaleAspectFill
        loginAnimationView.addSubview(animationView)
        animationView.play()
    }
    
    @IBAction func loginAction(_ sender: Any) {
        self.loginDelegate?.loginAction()
    }
}

protocol LoginDelegate {
    func loginAction()
}
