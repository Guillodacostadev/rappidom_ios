//
//  StoreView.swift
//  RappiDom
//
//  Created by Ricardo Grajales Duque on 10/20/18.
//  Copyright © 2018 Ricardo Grajales Duque. All rights reserved.
//

import UIKit

class StoreView: UIView {

    @IBOutlet var contentView: UIView!
    @IBOutlet var storeTableView: UITableView!
    let cellIdentifier = "storeCell"
    public var storeDelegate: StoreDelegate?
    private var storeArray: [Store]?
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setUpView()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setUpView()
    }
    
    private func setUpView() {
        
        Bundle.main.loadNibNamed("StoreView", owner: self, options: nil)
        addSubview(contentView)
        contentView.frame = self.bounds
        contentView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        
        storeTableView.delegate = self
        storeTableView.dataSource = self
        storeTableView.register(UITableViewCell.self, forCellReuseIdentifier: cellIdentifier)
        storeArray = StoreViewController.getStoresData()
        storeTableView.reloadData()
    }
}

extension StoreView: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return storeArray?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell: UITableViewCell = storeTableView.dequeueReusableCell(withIdentifier: cellIdentifier)!
        cell.textLabel?.text = storeArray?[indexPath.row].storeName
        cell.textLabel?.textColor = UIColor.black
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        storeDelegate?.goToCorridor(store: storeArray?[indexPath.row])
    }
}

protocol StoreDelegate {
    func goToCorridor(store: Store?)
}
