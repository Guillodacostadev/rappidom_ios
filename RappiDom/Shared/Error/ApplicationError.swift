//
//  ApplicationError.swift
//  RappiDom
//
//  Created by Guillermo Díaz Villegas
//  Copyright © 2018 Ricardo Grajales Duque. All rights reserved.
//

import Foundation

public enum ApplicationError : Error {
    case TechnicalError(String, Error?)
    case BussinessError((String, Int))
}


