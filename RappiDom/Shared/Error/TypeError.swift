//
//  TypeError.swift
//  RappiDom
//
//  Created by Guillermo Díaz Villegas
//  Copyright © 2018 Ricardo Grajales Duque. All rights reserved.
//

import Foundation
public enum TypeError {
    public static let defaultError = 0
    public static let unauthorizedOption = 1
    public static let notFound = 2
}
