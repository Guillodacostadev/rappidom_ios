//
//  Constans.swift
//  RappiDom
//
//  Created by Guillermo Díaz Villegas
//  Copyright © 2018 Ricardo Grajales Duque. All rights reserved.
//

import Foundation

enum Constants {
    
    static let BASE_USER = "https://shielded-river-49542.herokuapp.com/"
    static let BASE_IMAGES_LOW = "http://img.dev.rappi.com/products/low/"
    static let BASE_IMAGES_HIGH = "http://img.dev.rappi.com/products/high/"
    static let BASE_IMAGES_MEDIUM = "http://img.dev.rappi.com/products/medium/"
    static let BASE_PRODUCTS = "https://product-games.herokuapp.com/"
    static let BASE_CORRIDOR = "https://corridors-rappi-service.herokuapp.com/"
    static let BASE_STORES = "https://store-rappi-service.herokuapp.com/"
    static let BASE_SUBCORRIDOR = "https://sub-corridors-rappi-service.herokuapp.com/"
    
    static let URL_LOGIN = BASE_USER + "api/v1/login"
    static let URL_LOGOUT = BASE_USER + "api/v1/logout"
    static let URL_PRODUCTS_ORDERED = BASE_PRODUCTS + "api/v1/products"
    static let URL_PRODUCT = BASE_PRODUCTS + "api/v1/product"
    static let URL_PRODUCTS_NON_ORDERED = BASE_PRODUCTS + "api/v1/products-random"
    static let URL_CORRIDORS = BASE_CORRIDOR + "api/v1/corridors"
    static let URL_STORES = BASE_STORES + "api/v1/stores"
    static let URL_SUBCORRIDOR = BASE_SUBCORRIDOR + "api/v1/sub_corridors"
    static let URL_PRODUCT_DISLIKE = BASE_PRODUCTS + "api/v1/product-no-like"
    static let URL_PRODUCT_LIKE = BASE_PRODUCTS + "api/v1/product-like"
    static let URL_PRODUCT_SUPER_LIKE = BASE_PRODUCTS + "api/v1/product-super-like"
    static let URL_CORRIDOR_RANKING = BASE_CORRIDOR + "api/v1/corridors/favorites"
    static let URL_CORRIDOR = BASE_CORRIDOR + "api/v1/corridors"
    
}

