//
//  Messages.swift
//  RappiDom
//
//  Created by Guillermo Díaz Villegas
//  Copyright © 2018 Ricardo Grajales Duque. All rights reserved.
//

import Foundation

public enum Messages {
    static let BUSINESS_ERROR = "Bussiness error"
    static let GENERAL_ERROR = "___"
    static let CHECK_CONNECTION = "Por favor revise la conexión"
}
