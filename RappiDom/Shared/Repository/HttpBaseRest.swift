//  RappiDom
//
//  Created by Guillermo Díaz Villegas
//  Copyright © 2018 Ricardo Grajales Duque. All rights reserved.
//

import Foundation
import Alamofire

class HttpBaseRest {
    
    typealias commandError = () -> (String, Int)

    func validateResponse(response: DataResponse<Any>, commandError: commandError) throws {func validateResponse(response: DataResponse<Any>, commandError: commandError) throws {
            switch response.result {
            case .success:
                try validateStatusCode(response: response, commandError: commandError)
            case .failure(let error):
                try failure(error)
            }
        }
    }

    func validateStatusCode(response: DataResponse<Any>, commandError: commandError) throws {
        switch response.response?.statusCode {
        case 200?:
            break
        case 400?:
            printResponse(response: response)
            throw ApplicationError.BussinessError(commandError())
        default:
            printResponse(response: response)
            throw ApplicationError.BussinessError((Messages.BUSINESS_ERROR, TypeError.defaultError))
        }
    }
        
    func validateStatusCodeWithCode(response: DataResponse<Data>, commandError: commandError) throws {
        switch response.response?.statusCode {
        case 200?:
            break
        case 400?:
            printResponse(response: response)
            throw ApplicationError.BussinessError(commandError())
        default:
            printResponse(response: response)
            throw ApplicationError.BussinessError((Messages.BUSINESS_ERROR, TypeError.defaultError))
        }
    }

    func failure(_ error: Error) throws {
        print(error)
        var errorMessge = Messages.BUSINESS_ERROR
        if let urlError = error as? URLError {
            if urlError.code == .notConnectedToInternet {
                errorMessge = Messages.CHECK_CONNECTION
            }
        }
        throw ApplicationError.TechnicalError(errorMessge, error)
    }

    func printResponse(response: DataResponse<Any>) {
        print(response.result.value)
    }
        
    func printResponse(response: DataResponse<Data>) {
        print(response.result.value)
    }

    func getHeaderAutentication(token: String, id: String?) -> [String: String] {
        return [
//            "ContentType": "application/json",
//            "Authorization": "bearer \(token)",
            "token": token,
            "facebookID": id!
        ]
    }

    func getJsonHeader() -> [String: String] {
        return [
            "Content-Type": "application/json"
        ]
    }
    
    
    func getHeaderAutenticationLogout(token: String) -> [String: String] {
        return [
            "Authorization": "bearer \(token)"
        ]
    }

}
