//
//  GameDomain.swift
//  RappiDom
//
//  Created by Guillermo Díaz Villegas
//  Copyright © 2018 Ricardo Grajales Duque. All rights reserved.
//

import Foundation
import UIKit

public class GameDomain {
    private let gameRemoteRepository: GameRemoteRepository
    
    public init(gameRemoteRepository: GameRemoteRepository) {
        self.gameRemoteRepository = gameRemoteRepository
    }
    
    public func getProducts(with userProfile: UserProfile) throws -> [Product] {
        let products = try gameRemoteRepository.getProducts(with: userProfile)
        return products
    }
    
    public func rateProduct(with userProfile: UserProfile, animationType: AnimationType, product: Product) {
        do {
            switch animationType {
            case .like:
                try gameRemoteRepository.rateProductWithLike(with: userProfile, product: product)
                break
            case .superLike:
                try gameRemoteRepository.rateProductWithSuperLike(with: userProfile, product: product)
                break
            default:
                break
            }
        } catch let error {
            print(error)
        }
    }
    
    public func getImage(with imageName: String) -> UIImage? {
        var image: UIImage?
        do {
            try image = gameRemoteRepository.getImage(with: imageName)
        } catch let error {
            print(error)
            return nil
        }
        return image
    }
    
}
