//
//  GameRemoteRepository.swift
//  RappiDom
//
//  Created by Guillermo Díaz Villegas
//  Copyright © 2018 Ricardo Grajales Duque. All rights reserved.
//

import Foundation
import UIKit

public protocol GameRemoteRepository {
    
    func getProducts(with userProfile: UserProfile?) throws -> [Product]
    
    func rateProductWithLike(with userProfile: UserProfile?, product: Product)
    
    func rateProductWithSuperLike(with userProfile: UserProfile?, product: Product)
    
    func getImage(with imageName: String) -> UIImage?
    
}

