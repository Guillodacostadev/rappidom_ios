//
//  GameRemoteRepositoryImpl.swift
//  RappiDom
//
//  Created by Guillermo Díaz Villegas - Ceiba Software on 10/20/18.
//  Copyright © 2018 Ricardo Grajales Duque. All rights reserved.
//

import Alamofire
import Alamofire_Synchronous
import Foundation
import UIKit

class GameRemoteRepositoryImpl: HttpBaseRest, GameRemoteRepository {
    
    func getProducts(with userProfile: UserProfile?) throws -> [Product] {
        let response = Alamofire.request(Constants.URL_PRODUCTS_NON_ORDERED, method: HTTPMethod.get, parameters: nil, encoding: JSONEncoding.default, headers: getHeaderAutentication(token: userProfile!.facebookID ?? "", id: userProfile!.token)).responseJSON()
        let decoder = JSONDecoder()
        var root = RootProduct()
        root = try decoder.decode(RootProduct.self, from: response.data!)
        return root.products!
    }
    
    func rateProductWithLike(with userProfile: UserProfile?, product: Product) {
        let URL_LIKE = Constants.URL_PRODUCT_LIKE + "/" + String(product.productId!)
        do {
            try Alamofire.request(URL_LIKE, method: HTTPMethod.post, encoding: JSONEncoding.default, headers: getHeaderAutentication(token: userProfile!.facebookID ?? "", id: userProfile!.token)).responseJSON()
        } catch let error {
            print(error)
        }
    }
    
    func rateProductWithSuperLike(with userProfile: UserProfile?, product: Product) {
        let URL_LIKE = Constants.URL_PRODUCT_SUPER_LIKE + "/" + String(product.productId!)
        do {
            try Alamofire.request(URL_LIKE, method: HTTPMethod.post, encoding: JSONEncoding.default, headers: getHeaderAutentication(token: userProfile!.facebookID ?? "", id: userProfile!.token)).responseJSON()
        } catch let error {
            print(error)
        }
    }
    
    func getImage(with imageName: String) -> UIImage? {
        guard let url = URL(string: Constants.BASE_IMAGES_MEDIUM + imageName) else { return nil }
        var data: Data
        do {
            data = try Data(contentsOf: url)
        } catch {
            return nil
        }
        let image = UIImage.init(data: data)
        return image
    }
}
