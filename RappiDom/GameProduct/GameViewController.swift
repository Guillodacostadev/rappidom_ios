//
//  GameViewController.swift
//  RappiDom
//
//  Created by Ricardo Grajales Duque on 10/19/18.
//  Copyright © 2018 Ricardo Grajales Duque. All rights reserved.
//

import UIKit

class GameViewController: UIViewController, GameProductDelegate {
    
    @IBOutlet weak var gameProductView: GameProductView!
    var gameDomain: GameDomain!
    static var userProfile: UserProfile?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        gameProductView.gameProductDelegate = self
        gameDomain = GameDomain(gameRemoteRepository: GameRemoteRepositoryImpl())
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func endGameAction() {
        self.performSegue(withIdentifier: "storeViewController", sender: nil)
    }
    
    static func getProducs() -> [Product]? {
        var products: [Product]?
        let gameDomain = GameDomain(gameRemoteRepository: GameRemoteRepositoryImpl())
        do {
            try products = gameDomain.getProducts(with: GameViewController.userProfile ?? UserProfile())
        } catch let error {
            print(error)
        }
        return products
    }
    
    static func getImage(with imageName: String) -> UIImage? {
        var image: UIImage?
        do {
            let gameDomain = GameDomain(gameRemoteRepository: GameRemoteRepositoryImpl())
            image = try gameDomain.getImage(with: imageName)
        } catch let error {
            print(error)
            return nil
        }
        return image
    }
    
    func rateProduct(with userProfile: UserProfile, animationType: AnimationType, product: Product) {
        do {
            try self.gameDomain.rateProduct(with: userProfile, animationType: animationType, product: product)
        } catch let error {
            print(error)
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if (segue.identifier == "storeViewController") {
            let nextVc = segue.destination as? StoreViewController
            nextVc?.userProfile = nextVc?.userProfile
        }
    }

}
