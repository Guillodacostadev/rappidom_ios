//
//  GameViewController.swift
//  RappiDom
//
//  Created by Ricardo Grajales Duque on 10/19/18.
//  Copyright © 2018 Ricardo Grajales Duque. All rights reserved.
//

import UIKit

class GameViewController: UIViewController, GameProductDelegate {

    @IBOutlet weak var gameProductView: GameProductView!
    var userProfile: UserProfile?

    override func viewDidLoad() {
        super.viewDidLoad()
        gameProductView.gameProductDelegate = self
    }
    
    func endGameAction() {
        self.performSegue(withIdentifier: "storeViewController", sender: nil)
    }
    

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if (segue.identifier == "storeViewController") {
            let nextVc = segue.destination as? StoreViewController
            nextVc?.userProfile = userProfile
            nextVc?.storeView.storeDelegate = nextVc
        }
    }

}
