//
//  CorridorViewController.swift
//  RappiDom
//
//  Created by Ricardo Grajales Duque on 10/19/18.
//  Copyright © 2018 Ricardo Grajales Duque. All rights reserved.
//

import UIKit

class CorridorViewController: UIViewController {

    var userProfile: UserProfile?
    var currentStore: Store?

    override func viewDidLoad() {
        super.viewDidLoad()
        
        title = currentStore?.storeName
    }
    
    
}
