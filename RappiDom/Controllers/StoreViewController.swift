//
//  StoreViewController.swift
//  RappiDom
//
//  Created by Ricardo Grajales Duque on 10/20/18.
//  Copyright © 2018 Ricardo Grajales Duque. All rights reserved.
//

import UIKit

class StoreViewController: UIViewController, StoreDelegate {

    @IBOutlet weak var storeView: StoreView!
    var userProfile: UserProfile?
    var selectedStore: Store?

    override func viewDidLoad() {
        super.viewDidLoad()
        storeView.storeDelegate = self
    }

    
    static func getStoresData() -> [Store] {
     
        let store1 = Store()
        store1.storeName = "Tienda1"
        let store2 = Store()
        store2.storeName = "Tienda2"
        
        return [store1, store2]
    }
    
    func goToCorridor(store: Store?) {
        selectedStore = store
        performSegue(withIdentifier: "corridorViewController", sender: nil)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if (segue.identifier == "corridorViewController") {
            let nextVc = segue.destination as? CorridorViewController
            nextVc?.userProfile = userProfile
            nextVc?.currentStore = selectedStore
        }
    }

}
