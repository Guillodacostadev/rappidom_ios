//
//  LoginRemoteRepositoryImpl.swift
//  RappiDom
//
//  Created by Guillermo Díaz Villegas
//  Copyright © 2018 Ricardo Grajales Duque. All rights reserved.
//

import Alamofire
import Alamofire_Synchronous
import Foundation

class LoginRemoteRepositoryImpl: HttpBaseRest, LoginRemoteRepository {
    
    func login(with userProfile: UserProfile?) throws -> UserProfile {
        let response = Alamofire.request(Constants.URL_LOGIN, method: HTTPMethod.post, parameters: getParametersLogin(userProfile: userProfile!), encoding: JSONEncoding.default, headers: getJsonHeader()).responseJSON()
        
        let decoder = JSONDecoder()
//        let loginResponse = """
//            {
//                "userAge": "1414",
//                "userName": "bearer",
//                "facebookID": "3599",
//                "email": "true",
//                "token": "true"
//            }
//            """
        var rootx = Rootx()
        rootx = try decoder.decode(Rootx.self, from: response.data!)
        return rootx.user!
    }
    
    func logout(with userProfile: UserProfile) throws {
//
    }

}

extension LoginRemoteRepositoryImpl {
    
    fileprivate func getParametersLogin(userProfile: UserProfile) -> Parameters {
        let parameters: Parameters = [
            "userAge": userProfile.userAge,
            "userName": userProfile.userName,
            "facebookID": userProfile.facebookID,
            "email": userProfile.email,
            "token": userProfile.token
        ]
        return parameters
    }
}
