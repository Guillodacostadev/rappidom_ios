//
//  LoginRepository.swift
//  RappiDom
//
//  Created by Guillermo Díaz Villegas
//  Copyright © 2018 Ricardo Grajales Duque. All rights reserved.
//

import Foundation

public protocol LoginRemoteRepository {
    
    func login(with userProfile: UserProfile?) throws -> UserProfile
    
    func logout(with userProfile: UserProfile) throws
}

