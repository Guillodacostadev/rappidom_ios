//
//  UserProfile.swift
//  RappiDom
//
//  Created by Ricardo Grajales Duque on 10/19/18.
//  Copyright © 2018 Ricardo Grajales Duque. All rights reserved.
//

import Foundation

public class UserProfile: NSObject, Codable {
    
    var userName: String?
    var userAge: String?
    var facebookID: String?
    var email: String?
    var token: String?
}
