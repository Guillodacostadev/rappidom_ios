//
//  LoginDomain.swift
//  RappiDom
//
//  Created by Guillermo Díaz Villegas
//  Copyright © 2018 Ricardo Grajales Duque. All rights reserved.
//

import Foundation

public class LoginDomain {
    private let loginRemoteRepository: LoginRemoteRepository
    
    public init(loginRemoteRepository: LoginRemoteRepository) {
        self.loginRemoteRepository = loginRemoteRepository
    }
    
    public func login(with userProfile: UserProfile) throws {
        try loginRemoteRepository.login(with: userProfile)
    }
    
    public func logout(userProfile: UserProfile) throws {
        try loginRemoteRepository.logout(with: userProfile)
    }
}

