//
//  LoginViewController.swift
//  RappiDom
//
//  Created by Ricardo Grajales Duque on 10/19/18.
//  Copyright © 2018 Ricardo Grajales Duque. All rights reserved.
//

import UIKit
import FBSDKCoreKit
import FBSDKLoginKit

class LoginViewController: UIViewController, LoginDelegate {
    
    //MARK: Variables & Outlets
    
    @IBOutlet weak var loginView: LoginView!
    var loginDomain: LoginDomain!
    var userProfile: UserProfile?
    
    //MARK: Life Cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        loginView.loginDelegate = self
        checkFaceBookLogin()
    }
    
    private func checkFaceBookLogin() {
     
        if (FBSDKAccessToken.current() != nil) {
            getFacebookUserData()
            loginDomain = LoginDomain(loginRemoteRepository: LoginRemoteRepositoryImpl())
        }
    }
    
    //MARK: Login Functions
    
    func loginAction() {
        var loginManager: FBSDKLoginManager?
        loginManager = FBSDKLoginManager()
        loginManager?.logIn(withReadPermissions: ["public_profile", "email", "user_birthday"], from: self, handler: { (result, error) in

            if (error == nil) {
                let facebookLoginManager: FBSDKLoginManagerLoginResult? = result
                
                if (facebookLoginManager?.grantedPermissions != nil) {
                    
                    if ((facebookLoginManager?.grantedPermissions.contains("email"))!)
                    {
                        self.getFacebookUserData()
                    }
                }
            }
        })
    }
    
    private func getFacebookUserData() {
        if ((FBSDKAccessToken.current()) != nil) {
            FBSDKGraphRequest(graphPath: "me", parameters: ["fields": "id, name, first_name, last_name, picture.type(large), email, gender, birthday"]).start(completionHandler: { (connection, result, error) -> Void in
                if (error == nil) {
                    guard let userDataDictionary = result as? [AnyHashable: Any] else {
                        return
                    }
                    self.userProfile = UserProfile()
                    self.userProfile?.userName = userDataDictionary["name"] as? String
                    self.userProfile?.email = userDataDictionary["email"] as? String
                    self.userProfile?.facebookID = userDataDictionary["id"] as? String
                    self.userProfile?.userAge = self.getUserAge(ageBirthdayString: userDataDictionary["birthday"] as? String)
                    self.userProfile?.token = FBSDKAccessToken.current()?.tokenString
                    self.loginRepository()
                }
            })
        }
    }
    
    private func loginRepository() {
        do {
            try self.loginDomain.login(with: self.userProfile ?? UserProfile())
            self.performSegue(withIdentifier: "gameViewController", sender: nil)
        } catch let error {
            print(error)
        }
    }
    
    //MARK: Util Functions
    
    private func getUserAge(ageBirthdayString: String?) -> String? {
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "MM-dd-yyyy"
        dateFormatter.locale = Locale(identifier: "en_US_POSIX")
        let birthday = dateFormatter.date(from: ageBirthdayString!)
        let now = Date()
        let calendar = Calendar.current
        let ageComponents = calendar.dateComponents([.year], from: birthday!, to: now)
        let age = ageComponents.year!
        let ageString = String(age)
        
        return ageString
    }
    
    //MARK: Segues
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "gameViewController" {
//            let nextVc = segue.destination as? GameViewController
            GameViewController.userProfile = userProfile
        }
    }
    
}
