//
//  Root.swift
//  RappiDom
//
//  Created by Guillermo Díaz Villegas - Ceiba Software on 10/20/18.
//  Copyright © 2018 Ricardo Grajales Duque. All rights reserved.
//

import Foundation

public class Rootx: Codable {
    var status: String?
    var user: UserProfile?
    
    enum CodingKeys: String, CodingKey {
        case status
        case user
    }
}
