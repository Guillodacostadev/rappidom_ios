//
//  Store.swift
//  RappiDom
//
//  Created by Ricardo Grajales Duque on 10/20/18.
//  Copyright © 2018 Ricardo Grajales Duque. All rights reserved.
//

import Foundation

class Store: NSObject {
    var storeID: String?
    var storeName: String?
}
