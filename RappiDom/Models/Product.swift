//
//  Product.swift
//  RappiDom
//
//  Created by Ricardo Grajales Duque on 10/19/18.
//  Copyright © 2018 Ricardo Grajales Duque. All rights reserved.
//

import Foundation

public class Product: Codable {
    var descriptionString: String?
    var imageName: String?
    var name: String?
    var price: Int?
    var productId: Int?
    var storeId: Int?
    var corridors: [Corridor]?
    
    enum CodingKeys: String, CodingKey {
        case imageName = "image"
        case descriptionString = "description"
        case name = "name"
        case price = "price"
        case productId = "product_id"
        case storeId = "store_id"
        case corridors = "corridors"
    }
}
