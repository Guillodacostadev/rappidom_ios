//
//  Corridor.swift
//  RappiDom
//
//  Created by Guillermo Díaz Villegas
//  Copyright © 2018 Ricardo Grajales Duque. All rights reserved.
//

import Foundation

public class Corridor: Codable {
    
    var name: String?
    var idCorridor: Int?
    
    var icon: String?
    var imageSmall: String?
    var imageBig: String?
    var products: [Product]?
    
    enum CodingKeys: String, CodingKey {
        case name = "name"
        case idCorridor = "id"
        
        case icon = "icon"
        case imageBig = "image_big"
        case imageSmall = "image_small"
        
        case products = "products"
    }
}
