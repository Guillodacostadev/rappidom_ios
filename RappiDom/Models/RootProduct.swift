//
//  RootProduct.swift
//  RappiDom
//
//  Created by Guillermo Díaz Villegas
//  Copyright © 2018 Ricardo Grajales Duque. All rights reserved.
//

import Foundation

public class RootProduct: Codable {
    var status: String?
    var products: [Product]?
    
    enum CodingKeys: String, CodingKey {
        case status
        case products
    }
}
