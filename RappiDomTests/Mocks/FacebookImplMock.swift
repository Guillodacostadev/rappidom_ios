//
//  FacebookImplMock.swift
//  RappiDomTests
//
//

import Foundation
@testable import RappiDom

class FacebookImplMock {
    
    func auth() -> UserProfile {
        var userProfile = UserProfile()
        userProfile.userName = "Guillermo"
        userProfile.email = "srguille@gmail.com"
        userProfile.facebookID = "guillodiazvillegas"
        userProfile.userAge = "30"
        return userProfile
    }
}

